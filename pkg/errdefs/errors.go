package errdefs

import "errors"

var (
	ErrInvalidUserData = errors.New("data provided is invalid")
	ErrDatabaseFailed  = errors.New("a database error occurred")
	ErrUserNotFound    = errors.New("user not found")
)
