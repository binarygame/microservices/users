package constants

import "time"

// app config
const (
	ExpireDuration = 2 * time.Hour
)

// telemetry config
const (
	NamespaceKey           = "gitlab.com/binarygame/microservices/users"
	UsersCreatedCounterKey = "users_created"
)
