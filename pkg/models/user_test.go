package models_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/binarygame/microservices/users/pkg/models"
)

func TestNew(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		userNickname := "testuser"
		userEmoji := "😀"
		user, err := models.New(userNickname, userEmoji)

		assert.NoError(t, err)
		assert.NotNil(t, user)
		assert.Equal(t, userNickname, user.Nickname)
		assert.Equal(t, userEmoji, user.Emoji)
		assert.NotEmpty(t, user.ID)
	})

	t.Run("invalid emoji", func(t *testing.T) {
		// Invalid emoji
		userNickname := "testuser"
		userEmoji := "emoji"
		user, err := models.New(userNickname, userEmoji)

		assert.Error(t, err)
		assert.Nil(t, user)

		// Empty emoji
		userEmoji = ""
		user, err = models.New(userNickname, userEmoji)

		assert.Error(t, err)
		assert.Nil(t, user)
	})

	t.Run("invalid nickname", func(t *testing.T) {
		// Too big nickname
		userNickname := "testuserbignamelength"
		userEmoji := "😀"
		user, err := models.New(userNickname, userEmoji)

		assert.Error(t, err)
		assert.Nil(t, user)

		// Empty nickname
		userNickname = ""
		user, err = models.New(userNickname, userEmoji)

		assert.Error(t, err)
		assert.Nil(t, user)
	})
}

func TestNewWithId(t *testing.T) {
	userGen, err := models.New("testuser", "😀")
	assert.NoError(t, err)

	t.Run("success", func(t *testing.T) {
		user, err := models.NewWithId(userGen.ID, userGen.Nickname, userGen.Emoji, userGen.PrivateKey)

		assert.NoError(t, err)
		assert.NotNil(t, user)
		assert.Equal(t, userGen.ID, user.ID)
		assert.Equal(t, userGen.Nickname, user.Nickname)
		assert.Equal(t, userGen.Emoji, user.Emoji)
		assert.Equal(t, userGen.PrivateKey, user.PrivateKey)
	})

	t.Run("invalid emoji", func(t *testing.T) {
		// Invalid emoji
		user, err := models.NewWithId(userGen.ID, userGen.Nickname, "emoji", userGen.PrivateKey)

		assert.Error(t, err)
		assert.Nil(t, user)

		// Empty emoji
		user, err = models.NewWithId(userGen.ID, userGen.Nickname, "", userGen.PrivateKey)

		assert.Error(t, err)
		assert.Nil(t, user)
	})

	t.Run("invalid nickname", func(t *testing.T) {
		// Too big nickname
		user, err := models.NewWithId(userGen.ID, "testuserbignamelength", userGen.Emoji, userGen.PrivateKey)

		assert.Error(t, err)
		assert.Nil(t, user)

		// Empty nickname
		user, err = models.NewWithId(userGen.ID, "", userGen.Emoji, userGen.PrivateKey)

		assert.Error(t, err)
		assert.Nil(t, user)
	})
}
