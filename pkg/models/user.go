package models

import (
	"github.com/go-playground/validator/v10"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
)

var ValidEmojis = []string{
	"😀", "😁", "😆", "🤣", "😉", "🤩", "🤪", "🤔", "😎",
	"😮", "👱", "🧔", "👨‍🦱", "🧑‍🦳", "🧑‍🦰", "👨‍🦲", "👩", "👩‍🦰",
	"👩‍🦳", "👩‍🦱", "👩‍🦲", "👱‍♀️", "👵", "👴", "🦁", "🐯", "🦊",
	"🤡", "👻", "👽", "👾", "🤖", "😺", "🐵", "🐶", "🦄", "🐝",
	"🐺", "🦝", "🐧", "🐔", "🦉", "🐸", "🐢", "🧠", "👀",
}

var modelValidator *validator.Validate = validator.New()

type User struct {
	ID         string `validate:"required,uuid5"`
	Nickname   string `validate:"required,lte=20" redis:"nickname"`
	Emoji      string `validate:"allowedEmoji" redis:"emoji"`
	PrivateKey string `validate:"omitempty,sha256"`
}

func init() {
	modelValidator.RegisterValidation("allowedEmoji", ValidateEmoji)
}

func New(userNickname string, userEmoji string) (*User, error) {
	privateKey, err := bingameutils.GeneratePrivateKey()
	if err != nil {
		return nil, err
	}

	userId := bingameutils.DerivePublicUUID(privateKey)
	u := &User{
		ID:         userId.String(),
		Nickname:   userNickname,
		Emoji:      userEmoji,
		PrivateKey: privateKey,
	}

	err = u.Validate()
	if err != nil {
		return nil, err
	}

	return u, nil
}

func NewWithId(userID string, userNickname string, userEmoji string, privateKey string) (*User, error) {
	u := &User{
		ID:         userID,
		Nickname:   userNickname,
		Emoji:      userEmoji,
		PrivateKey: privateKey,
	}

	err := u.Validate()
	if err != nil {
		return nil, err
	}

	return u, nil
}

// ValidateEmoji checks if the input string is a valid emoji,
// based on a predefined list of valid emojis.
func ValidateEmoji(fl validator.FieldLevel) bool {
	emoji := string(fl.Field().String())

	for _, validEmoji := range ValidEmojis {
		if emoji == validEmoji {
			return true
		}
	}

	return false
}

// Validate checks if the User struct is valid.
func (u *User) Validate() error {
	return modelValidator.Struct(u)
}
