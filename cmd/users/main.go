package main

import (
	"context"
	"errors"
	"log/slog"

	"github.com/joho/godotenv"
	"gitlab.com/binarygame/microservices/users/internal/repository"
	service "gitlab.com/binarygame/microservices/users/internal/server"
	"gitlab.com/binarygame/microservices/users/internal/usecase"
	"gitlab.com/binarygame/microservices/users/pkg/constants"

	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		slog.Info("Error loading .env file")
	}

	logger := bingameutils.GetLogger()
	slog.SetDefault(logger)

	serviceHost := bingameutils.GetServiceHost()
	servicePort := bingameutils.GetServicePort()

	// Set up OpenTelemetry.
	ts, otelShutdown, err := telemetry.New(context.Background(), constants.NamespaceKey)
	if err != nil {
		slog.Error("failed to set up OpenTelemetry SDK", "error", err)
	}
	defer func() {
		err = errors.Join(err, otelShutdown(context.Background()))
	}()

	repository := repository.New(bingameutils.GetValkeyAddress())
	usecase := usecase.New(repository, logger, ts)

	service.Start(serviceHost, servicePort, usecase, logger, ts)
}
