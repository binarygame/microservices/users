FROM golang:1.22.4 AS build-stage

WORKDIR /usr/src/app

COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY cmd ./cmd
COPY internal ./internal
COPY pkg ./pkg

RUN CGO_ENABLED=0 GOOS=linux go build -v -o /usr/local/bin/app ./cmd/users/main.go

FROM debian:bookworm-slim

WORKDIR /

COPY --from=build-stage /usr/local/bin/app /app

ENTRYPOINT ["/app"]
