package service

import (
	"context"
	"log/slog"
	"net/http"
	"strconv"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/users/internal/usecase"
	"gitlab.com/binarygame/microservices/users/pkg/constants"
	"gitlab.com/binarygame/microservices/users/pkg/errdefs"
	usersv1 "gitlab.com/binarygame/microservices/users/pkg/gen/connectrpc/users/v1"
	"gitlab.com/binarygame/microservices/users/pkg/gen/connectrpc/users/v1/usersv1connect"

	"connectrpc.com/connect"
	"connectrpc.com/grpcreflect"
	"connectrpc.com/otelconnect"
	bingameutils "gitlab.com/binarygame/microservices/bingame-utils"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

// ErrorCodeMap matches errors to connect codes
var errorCodeMap = map[error]connect.Code{
	errdefs.ErrInvalidUserData: connect.CodeInvalidArgument,
	errdefs.ErrDatabaseFailed:  connect.CodeInternal,
	errdefs.ErrUserNotFound:    connect.CodeNotFound,
}

// getConnectCodeFromError is a helper to get the connect code from an error
func getConnectCodeFromError(err error) connect.Code {
	if code, exists := errorCodeMap[err]; exists {
		return code
	}
	return connect.CodeUnknown
}

type UsersServer struct {
	usersv1connect.UnimplementedUsersServiceHandler
	usecase usecase.Users
	logger  *slog.Logger
}

func newUsersServer(usecase usecase.Users, logger *slog.Logger) *UsersServer {
	return &UsersServer{
		usecase: usecase,
		logger:  logger,
	}
}

func Start(ip string, port int, usecase usecase.Users, logger *slog.Logger, ts telemetry.TelemetryService) {
	addr := ip + ":" + strconv.Itoa(port)

	ts.NewCounter(constants.UsersCreatedCounterKey, "Number of users created", telemetry.IntCounterType)

	otelInterceptor, err := otelconnect.NewInterceptor(otelconnect.WithTrustRemote())
	if err != nil {
		logger.Error("An error occurred in otelconnect.NewInterceptor", "error", err)
	}

	server := newUsersServer(usecase, logger)
	mux := http.NewServeMux()
	path, handler := usersv1connect.NewUsersServiceHandler(
		server,
		connect.WithInterceptors(otelInterceptor),
	)

	// For GRPC reflection
	reflector := grpcreflect.NewStaticReflector(
		usersv1connect.UsersServiceName,
	)

	mux.Handle(path, handler)
	mux.Handle(grpcreflect.NewHandlerV1(reflector))
	mux.Handle(grpcreflect.NewHandlerV1Alpha(reflector))

	logger.Info("Starting connect server for binarygame-users", "ip", ip, "port", port)

	err = http.ListenAndServe(
		addr,
		h2c.NewHandler(mux, &http2.Server{}),
	)
	if err != nil {
		logger.Error("An error occurred in http.ListenAndServe", "error", err)
	}
}

func (us *UsersServer) AddUser(ctx context.Context, r *connect.Request[usersv1.AddUserRequest]) (*connect.Response[usersv1.AddUserResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	data, err := us.usecase.Create(ctx, r.Msg.Nickname, r.Msg.Emoji)
	if err != nil {
		connectErr := connect.NewError(getConnectCodeFromError(err), err)
		us.logger.Debug("Returning error response", "error", connectErr)
		return nil, connectErr
	}

	return connect.NewResponse(&usersv1.AddUserResponse{
		Data: &usersv1.AddUserData{Id: data.ID, PrivateKey: data.PrivateKey},
	}), nil
}

func (us *UsersServer) EditUser(ctx context.Context, r *connect.Request[usersv1.EditUserRequest]) (*connect.Response[usersv1.EditUserResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())

	err := us.usecase.Edit(ctx, r.Msg.UserId, r.Msg.Nickname, r.Msg.Emoji)
	if err != nil {
		connectErr := connect.NewError(getConnectCodeFromError(err), err)
		us.logger.Debug("Returning error response", "error", connectErr)
		return nil, connectErr
	}

	return connect.NewResponse(&usersv1.EditUserResponse{}), nil
}

func (us *UsersServer) GetAllowedEmojis(ctx context.Context, r *connect.Request[usersv1.GetAllowedEmojisRequest]) (*connect.Response[usersv1.GetAllowedEmojisResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	allowedEmoji, err := us.usecase.AllowedEmojis(ctx)
	if err != nil {
		return nil, connect.NewError(connect.CodeUnknown, err)
	}

	return connect.NewResponse(&usersv1.GetAllowedEmojisResponse{
		Data: allowedEmoji,
	}), nil
}

func (us *UsersServer) GetUser(ctx context.Context, r *connect.Request[usersv1.GetUserRequest]) (*connect.Response[usersv1.GetUserResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	user, err := us.usecase.Get(ctx, r.Msg.GetUserId())
	if err != nil {
		connectErr := connect.NewError(getConnectCodeFromError(err), err)
		us.logger.Debug("Returning error response", "error", connectErr)
		return nil, connectErr
	}

	return &connect.Response[usersv1.GetUserResponse]{
		Msg: &usersv1.GetUserResponse{
			Nickname: user.Nickname,
			Emoji:    user.Emoji,
		},
	}, nil
}

func (us *UsersServer) GetUsers(ctx context.Context, r *connect.Request[usersv1.GetUsersRequest]) (*connect.Response[usersv1.GetUsersResponse], error) {
	ctx = bingameutils.GetCtxWithCredentialsFromHeader(ctx, r.Header())
	// Extract user IDs from the request
	userIds := r.Msg.GetUserIds()

	// Call the usecase to get multiple users
	users, err := us.usecase.GetMultiple(ctx, userIds)
	if err != nil {
		connectErr := connect.NewError(getConnectCodeFromError(err), err)
		us.logger.Debug("Returning error response", "error", connectErr)
		return nil, connectErr
	}

	// Prepare the response
	responseUsers := make(map[string]*usersv1.GetUsersResponse_User)
	for _, user := range users {
		responseUsers[user.ID] = &usersv1.GetUsersResponse_User{
			Nickname: user.Nickname,
			Emoji:    user.Emoji,
		}
	}

	return connect.NewResponse(&usersv1.GetUsersResponse{
		Users: responseUsers,
	}), nil
}
