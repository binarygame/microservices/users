package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	model "gitlab.com/binarygame/microservices/users/pkg/models"
)

type MockUsersUsecase struct {
	mock.Mock
}

func (m *MockUsersUsecase) Create(ctx context.Context, nickname, emoji string) (*model.User, error) {
	args := m.Called(ctx, nickname, emoji)
	if args.Get(0) != nil {
		return args.Get(0).(*model.User), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockUsersUsecase) Edit(ctx context.Context, userId, nickname, emoji string) error {
	args := m.Called(ctx, userId, nickname, emoji)
	return args.Error(0)
}

func (m *MockUsersUsecase) AllowedEmojis(ctx context.Context) ([]string, error) {
	args := m.Called(ctx)
	return args.Get(0).([]string), args.Error(1)
}

func (m *MockUsersUsecase) Get(ctx context.Context, userId string) (*model.User, error) {
	args := m.Called(ctx, userId)
	if args.Get(0) != nil {
		return args.Get(0).(*model.User), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockUsersUsecase) GetMultiple(ctx context.Context, userIds []string) ([]*model.User, error) {
	args := m.Called(ctx, userIds)
	if args.Get(0) != nil {
		return args.Get(0).([]*model.User), args.Error(1)
	}
	return nil, args.Error(1)
}
