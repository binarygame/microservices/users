package service

import (
	"bytes"
	"context"
	"log/slog"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"connectrpc.com/connect"
	"gitlab.com/binarygame/microservices/users/internal/server/mocks"
	"gitlab.com/binarygame/microservices/users/pkg/errdefs"
	usersv1 "gitlab.com/binarygame/microservices/users/pkg/gen/connectrpc/users/v1"
	"gitlab.com/binarygame/microservices/users/pkg/gen/connectrpc/users/v1/usersv1connect"
	"gitlab.com/binarygame/microservices/users/pkg/models"
)

func initTestServer() (*httptest.Server, *mocks.MockUsersUsecase) {
	var loggerOutput bytes.Buffer
	logger := slog.New(slog.NewTextHandler(&loggerOutput, nil))
	mockUsecase := new(mocks.MockUsersUsecase)
	service := newUsersServer(mockUsecase, logger)

	mux := http.NewServeMux()
	mux.Handle(usersv1connect.NewUsersServiceHandler(service))

	testServer := httptest.NewUnstartedServer(mux)
	testServer.EnableHTTP2 = true
	return testServer, mockUsecase
}

func initClients(testServer *httptest.Server) []usersv1connect.UsersServiceClient {
	connectClient := usersv1connect.NewUsersServiceClient(
		testServer.Client(),
		testServer.URL,
	)
	connectGrpcClient := usersv1connect.NewUsersServiceClient(
		testServer.Client(),
		testServer.URL,
		connect.WithGRPC(),
	)
	return []usersv1connect.UsersServiceClient{connectClient, connectGrpcClient}
}

func TestAddUser(t *testing.T) {
	t.Parallel()

	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	validUser, _ := models.New("test", "🦝")

	t.Run("success", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Create", mock.Anything, "test", "🦝").Return(validUser, nil).Once()

			result, err := client.AddUser(
				context.Background(),
				connect.NewRequest(&usersv1.AddUserRequest{
					Nickname: "test",
					Emoji:    "🦝",
				}),
			)
			require.Nil(t, err)
			assert.Equal(t, validUser.ID, result.Msg.Data.Id)
			assert.Equal(t, validUser.PrivateKey, result.Msg.Data.PrivateKey)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("fail", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Create", mock.Anything, "test", "emoji").Return(nil, assert.AnError).Once()

			_, err := client.AddUser(
				context.Background(),
				connect.NewRequest(&usersv1.AddUserRequest{
					Nickname: "test",
					Emoji:    "emoji",
				}),
			)
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestEditUser(t *testing.T) {
	t.Parallel()

	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	t.Run("success", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Edit", mock.Anything, "b65febd2-b5d4-50b3-b7e6-d224164f982f", "test", "🦝").Return(nil).Once()

			_, err := client.EditUser(
				context.Background(),
				connect.NewRequest(&usersv1.EditUserRequest{
					UserId:   "b65febd2-b5d4-50b3-b7e6-d224164f982f",
					Nickname: "test",
					Emoji:    "🦝",
				}),
			)
			require.NoError(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("fail", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Edit", mock.Anything, "b65febd2-b5d4-50b3-b7e6-d224164f982f", "test", "emoji").Return(assert.AnError).Once()

			_, err := client.EditUser(
				context.Background(),
				connect.NewRequest(&usersv1.EditUserRequest{
					UserId:   "b65febd2-b5d4-50b3-b7e6-d224164f982f",
					Nickname: "test",
					Emoji:    "emoji",
				}),
			)
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestGetAllowedEmojis(t *testing.T) {
	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	t.Run("success", func(t *testing.T) {
		mockUsecase.On("AllowedEmojis", mock.Anything).Return(models.ValidEmojis, nil)

		for _, client := range clients {
			result, err := client.GetAllowedEmojis(
				context.Background(),
				connect.NewRequest(&usersv1.GetAllowedEmojisRequest{}),
			)
			require.NoError(t, err)
			assert.Equal(t, models.ValidEmojis, result.Msg.Data)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestGetUser(t *testing.T) {
	t.Parallel()

	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	validUser, _ := models.New("test", "🦝")
	validUser.ID = "b65febd2-b5d4-50b3-b7e6-d224164f982f"

	t.Run("success", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Get", mock.Anything, "b65febd2-b5d4-50b3-b7e6-d224164f982f").Return(validUser, nil).Once()

			result, err := client.GetUser(
				context.Background(),
				connect.NewRequest(&usersv1.GetUserRequest{
					UserId: "b65febd2-b5d4-50b3-b7e6-d224164f982f",
				}),
			)
			require.NoError(t, err)
			assert.Equal(t, validUser.Nickname, result.Msg.Nickname)
			assert.Equal(t, validUser.Emoji, result.Msg.Emoji)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("user not found", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Get", mock.Anything, "b65febd2-b5d4-50b3-b7e6-d224164f982f").Return(nil, errdefs.ErrUserNotFound).Once()

			_, err := client.GetUser(
				context.Background(),
				connect.NewRequest(&usersv1.GetUserRequest{
					UserId: "b65febd2-b5d4-50b3-b7e6-d224164f982f",
				}),
			)
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("database fail", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("Get", mock.Anything, "b65febd2-b5d4-50b3-b7e6-d224164f982f").Return(nil, errdefs.ErrDatabaseFailed).Once()

			_, err := client.GetUser(
				context.Background(),
				connect.NewRequest(&usersv1.GetUserRequest{
					UserId: "b65febd2-b5d4-50b3-b7e6-d224164f982f",
				}),
			)
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})
}

func TestGetUsers(t *testing.T) {
	t.Parallel()

	testServer, mockUsecase := initTestServer()
	testServer.StartTLS()
	defer testServer.Close()

	clients := initClients(testServer)

	validUser1 := &models.User{
		ID:       "testid1",
		Nickname: "testnick1",
		Emoji:    models.ValidEmojis[0],
	}
	validUser2 := &models.User{
		ID:       "testid2",
		Nickname: "testnick2",
		Emoji:    models.ValidEmojis[1],
	}
	userIds := []string{validUser1.ID, validUser2.ID}

	t.Run("success", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("GetMultiple", mock.Anything, userIds).Return([]*models.User{validUser1, validUser2}, nil).Once()

			result, err := client.GetUsers(
				context.Background(),
				connect.NewRequest(&usersv1.GetUsersRequest{
					UserIds: userIds,
				}),
			)
			require.NoError(t, err)
			assert.Equal(t, validUser1.Nickname, result.Msg.Users[validUser1.ID].Nickname)
			assert.Equal(t, validUser1.Emoji, result.Msg.Users[validUser1.ID].Emoji)
			assert.Equal(t, validUser2.Nickname, result.Msg.Users[validUser2.ID].Nickname)
			assert.Equal(t, validUser2.Emoji, result.Msg.Users[validUser2.ID].Emoji)
		}
		mockUsecase.AssertExpectations(t)
	})

	t.Run("database fail", func(t *testing.T) {
		for _, client := range clients {
			mockUsecase.On("GetMultiple", mock.Anything, userIds).Return(nil, errdefs.ErrDatabaseFailed).Once()

			_, err := client.GetUsers(
				context.Background(),
				connect.NewRequest(&usersv1.GetUsersRequest{
					UserIds: userIds,
				}),
			)
			assert.Error(t, err)
		}
		mockUsecase.AssertExpectations(t)
	})
}
