package repository

const usersRedisKey = "users"

func buildUserKey(userID string) string {
	return usersRedisKey + ":" + userID
}
