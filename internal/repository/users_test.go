package repository_test

import (
	"context"
	"testing"

	"github.com/alicebob/miniredis/v2"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"gitlab.com/binarygame/microservices/users/internal/repository"
	"gitlab.com/binarygame/microservices/users/pkg/errdefs"
	model "gitlab.com/binarygame/microservices/users/pkg/models"
)

const usersRedisKey = "users"

func setupTestRedis(t *testing.T) (*redis.Client, *miniredis.Miniredis) {
	s := miniredis.RunT(t)

	client := redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})

	return client, s
}

func TestInsertUser(t *testing.T) {
	t.Run("database fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		user := &model.User{ID: "1", Nickname: "testuser", Emoji: "👀"}
		err := repo.InsertUser(ctx, user)
		assert.Error(t, err)
	})

	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		userId := "1"

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		user := &model.User{ID: userId, Nickname: "testuser", Emoji: "👀"}
		err := repo.InsertUser(ctx, user)
		assert.NoError(t, err)

		exists, err := repo.UserExists(ctx, userId)
		assert.NoError(t, err)
		assert.True(t, exists)

		queryUser, err := repo.GetUserById(ctx, userId)
		assert.NoError(t, err)
		assert.Equal(t, user, queryUser)
	})
}

func TestUpdateUser(t *testing.T) {
	t.Run("database fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		user := &model.User{ID: "1", Nickname: "testuser", Emoji: "👀"}

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		// Simulate Redis failure
		s.Close()

		err := repo.UpdateUser(ctx, user)
		assert.Error(t, err)
	})

	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		userId := "1"

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		user := &model.User{ID: userId, Nickname: "testuser", Emoji: "👀"}
		_ = repo.InsertUser(ctx, user)

		queryUser, err := repo.GetUserById(ctx, userId)
		assert.NoError(t, err)

		queryUser.Nickname = "newnickname"
		queryUser.Emoji = "😎"

		err = repo.UpdateUser(ctx, queryUser)
		assert.NoError(t, err)

		updatedUser, err := repo.GetUserById(ctx, userId)
		assert.NoError(t, err)

		assert.Equal(t, queryUser, updatedUser)
	})
}

func TestGetUserById(t *testing.T) {
	t.Run("user not found", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		user, err := repo.GetUserById(ctx, "1")
		assert.ErrorIs(t, err, errdefs.ErrUserNotFound)
		assert.Nil(t, user)
	})

	t.Run("database fail", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		userId := "1"

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		user := &model.User{ID: userId, Nickname: "testuser", Emoji: "👀"}
		_ = repo.InsertUser(ctx, user)

		// Simulate Redis failure
		s.Close()

		_, err := repo.GetUserById(ctx, userId)
		assert.Error(t, err)
	})

	t.Run("success", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		userId := "1"

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		user := &model.User{ID: userId, Nickname: "testuser", Emoji: "👀"}
		_ = repo.InsertUser(ctx, user)

		retrievedUser, err := repo.GetUserById(ctx, userId)
		assert.NoError(t, err)
		assert.Equal(t, user, retrievedUser)
	})
}

func TestUserExistsFalse(t *testing.T) {
	t.Run("false", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		exists, err := repo.UserExists(ctx, "nonexistent")
		assert.NoError(t, err)
		assert.False(t, exists)
	})

	t.Run("true", func(t *testing.T) {
		client, s := setupTestRedis(t)
		defer s.Close()

		userId := "1"

		repo := repository.New(client.Options().Addr)
		ctx := context.Background()

		user := &model.User{ID: userId, Nickname: "testuser", Emoji: "👀"}
		_ = repo.InsertUser(ctx, user)

		exists, err := repo.UserExists(ctx, userId)
		assert.NoError(t, err)
		assert.True(t, exists)
	})
}
