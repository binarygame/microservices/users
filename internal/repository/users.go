package repository

import (
	"context"

	"gitlab.com/binarygame/microservices/users/pkg/constants"
	"gitlab.com/binarygame/microservices/users/pkg/errdefs"
	model "gitlab.com/binarygame/microservices/users/pkg/models"

	"github.com/redis/go-redis/extra/redisotel/v9"
	"github.com/redis/go-redis/v9"
)

type UsersRepository interface {
	InsertUser(ctx context.Context, user *model.User) error
	UpdateUser(ctx context.Context, user *model.User) error
	GetUserById(ctx context.Context, id string) (*model.User, error)
	GetUsersByIds(ctx context.Context, ids []string) ([]*model.User, error)
	UserExists(ctx context.Context, id string) (bool, error)
}

type usersRepository struct {
	db *redis.Client
}

func New(address string) *usersRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// Enable tracing instrumentation.
	if err := redisotel.InstrumentTracing(client); err != nil {
		panic(err)
	}

	// Enable metrics instrumentation.
	if err := redisotel.InstrumentMetrics(client); err != nil {
		panic(err)
	}

	return &usersRepository{
		db: client,
	}
}

func (g *usersRepository) InsertUser(ctx context.Context, user *model.User) error {
	key := usersRedisKey + ":" + user.ID

	_, err := g.db.HSet(ctx, key, user).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *usersRepository) UpdateUser(ctx context.Context, user *model.User) error {
	key := buildUserKey(user.ID)

	_, err := g.db.HSet(ctx, key, user).Result()
	if err != nil {
		return err
	}

	err = g.db.Expire(ctx, key, constants.ExpireDuration).Err()
	if err != nil {
		return err
	}

	return nil
}

func (g *usersRepository) GetUserById(ctx context.Context, id string) (*model.User, error) {
	key := buildUserKey(id)

	var user model.User

	res := g.db.HGetAll(ctx, key)

	val, err := res.Result()
	if err != nil {
		return nil, err
	}
	if len(val) == 0 { // user not found
		return nil, errdefs.ErrUserNotFound
	}

	err = res.Scan(&user)
	if err != nil {
		return nil, err
	}

	user.ID = id // id is not stored in the hash

	return &user, nil
}

// Gets multiple users' data at once.
//
// Returns an array of users found. If a user does not exists,
// it is simply excluded from the result. No error is returned
func (g *usersRepository) GetUsersByIds(ctx context.Context, ids []string) ([]*model.User, error) {
	if len(ids) == 0 {
		return []*model.User{}, nil
	}

	// Use a pipeline to execute multiple commands concurrently
	pipe := g.db.Pipeline()
	cmds := make([]*redis.MapStringStringCmd, len(ids))

	for i, id := range ids {
		key := buildUserKey(id)
		cmds[i] = pipe.HGetAll(ctx, key)
	}

	// Execute all the commands in the pipeline
	_, err := pipe.Exec(ctx)
	if err != nil && err != redis.Nil {
		return nil, err
	}

	users := make([]*model.User, 0, len(ids))
	for i, cmd := range cmds {
		val, err := cmd.Result()
		if err != nil && err != redis.Nil {
			return nil, err
		}
		if len(val) == 0 { // user not found
			continue // skip this user
		}

		var user model.User
		err = cmd.Scan(&user)
		if err != nil {
			return nil, err
		}
		user.ID = ids[i] // id is not stored in the hash
		users = append(users, &user)
	}

	return users, nil
}

func (g *usersRepository) UserExists(ctx context.Context, id string) (bool, error) {
	key := buildUserKey(id)

	exists, err := g.db.Exists(ctx, key).Result()
	if err != nil {
		return false, err
	}

	return exists == 1, nil
}
