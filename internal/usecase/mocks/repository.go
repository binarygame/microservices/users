package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	model "gitlab.com/binarygame/microservices/users/pkg/models"
)

type MockUsersRepository struct {
	mock.Mock
}

func (m *MockUsersRepository) InsertUser(ctx context.Context, user *model.User) error {
	args := m.Called(ctx, user)
	return args.Error(0)
}

func (m *MockUsersRepository) UpdateUser(ctx context.Context, user *model.User) error {
	args := m.Called(ctx, user)
	return args.Error(0)
}

func (m *MockUsersRepository) GetUserById(ctx context.Context, id string) (*model.User, error) {
	args := m.Called(ctx, id)
	if args.Get(0) != nil {
		return args.Get(0).(*model.User), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockUsersRepository) GetUsersByIds(ctx context.Context, ids []string) ([]*model.User, error) {
	args := m.Called(ctx, ids)
	if args.Get(0) != nil {
		return args.Get(0).([]*model.User), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *MockUsersRepository) UserExists(ctx context.Context, id string) (bool, error) {
	args := m.Called(ctx, id)
	return args.Bool(0), args.Error(1)
}
