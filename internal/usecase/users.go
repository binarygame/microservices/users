package usecase

import (
	"context"
	"errors"
	"fmt"
	"log/slog"

	"gitlab.com/binarygame/microservices/bingame-utils/telemetry"
	"gitlab.com/binarygame/microservices/users/internal/repository"
	"gitlab.com/binarygame/microservices/users/pkg/constants"
	"gitlab.com/binarygame/microservices/users/pkg/errdefs"
	model "gitlab.com/binarygame/microservices/users/pkg/models"
)

type Users interface {
	Create(ctx context.Context, userNickname string, userEmoji string) (*model.User, error)
	Edit(ctx context.Context, userId string, nickname string, emoji string) error
	AllowedEmojis(ctx context.Context) ([]string, error)
	Get(ctx context.Context, userId string) (*model.User, error)
	GetMultiple(ctx context.Context, userIds []string) ([]*model.User, error)
}

type users struct {
	repository repository.UsersRepository
	logger     *slog.Logger
	telemetry  telemetry.TelemetryService
}

func New(repo repository.UsersRepository, logger *slog.Logger, telemetry telemetry.TelemetryService) Users {
	return &users{
		repository: repo,
		logger:     logger,
		telemetry:  telemetry,
	}
}

func (uu *users) Create(ctx context.Context, nickname string, emoji string) (*model.User, error) {
	// New grouped logger
	logger := uu.logger.With(slog.Group("create", "nickname", nickname, "emoji", emoji))

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "Create called")

	span := uu.telemetry.Record(ctx, "create",
		map[string]string{
			"nickname": nickname,
			"emoji":    emoji,
		})
	defer span.End()

	user, err := model.New(nickname, emoji)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelDebug, "Received invalid user data", slog.String("error", err.Error()))
		return nil, errdefs.ErrInvalidUserData
	}

	err = uu.repository.InsertUser(ctx, user)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Error inserting user into database", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	uu.telemetry.IncrementCounter(ctx, constants.UsersCreatedCounterKey, 1)

	return user, nil
}

func (uu *users) Edit(ctx context.Context, userId, nickname, emoji string) error {
	// New grouped logger
	logger := uu.logger.With(slog.Group("edit", "userId", userId, "nickname", nickname, "emoji", emoji))

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "Edit called")

	span := uu.telemetry.Record(ctx, "edit",
		map[string]string{
			"userId":   userId,
			"nickname": nickname,
			"emoji":    emoji,
		})
	defer span.End()

	user, err := uu.repository.GetUserById(ctx, userId)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Error getting user from database", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	if user == nil {
		logger.LogAttrs(ctx, slog.LevelError, "User not found")
		return errdefs.ErrUserNotFound
	}

	user.Nickname = nickname
	user.Emoji = emoji

	err = user.Validate()
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelDebug, "Received invalid user data", slog.String("error", err.Error()))
		return errdefs.ErrInvalidUserData
	}

	err = uu.repository.UpdateUser(ctx, user)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Error updating user in database", slog.String("error", err.Error()))
		return errdefs.ErrDatabaseFailed
	}

	return nil
}

func (uu *users) AllowedEmojis(ctx context.Context) ([]string, error) {
	// New grouped logger
	logger := uu.logger.With(slog.Group("allowed_emojis"))

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "AllowedEmojis called")

	span := uu.telemetry.Record(ctx, "allowed_emojis", nil)
	defer span.End()

	return model.ValidEmojis, nil
}

func (uu *users) Get(ctx context.Context, userId string) (*model.User, error) {
	// New grouped logger
	logger := uu.logger.With(slog.Group("get", "userId", userId))

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "Get called")

	span := uu.telemetry.Record(ctx, "get",
		map[string]string{
			"userId": userId,
		})
	defer span.End()

	user, err := uu.repository.GetUserById(ctx, userId)
	if err != nil {
		if errors.Is(err, errdefs.ErrUserNotFound) {
			logger.LogAttrs(ctx, slog.LevelInfo, "User does not exist", slog.String("error", err.Error()))
			return nil, err
		}

		logger.LogAttrs(ctx, slog.LevelError, "Error getting user from database", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	return user, nil
}

func (uu *users) GetMultiple(ctx context.Context, userIds []string) ([]*model.User, error) {
	// New grouped logger
	logger := uu.logger.With(slog.Group("get_multiple", "userIds", userIds))

	// Debug log for function call
	logger.LogAttrs(ctx, slog.LevelDebug, "GetMultiple called")

	span := uu.telemetry.Record(ctx, "get_multiple",
		map[string]string{
			"userIds": fmt.Sprint(userIds),
		})
	defer span.End()

	users, err := uu.repository.GetUsersByIds(ctx, userIds)
	if err != nil {
		logger.LogAttrs(ctx, slog.LevelError, "Error getting multiple users from database", slog.String("error", err.Error()))
		return nil, errdefs.ErrDatabaseFailed
	}

	return users, nil
}
