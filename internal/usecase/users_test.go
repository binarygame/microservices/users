package usecase_test

import (
	"bytes"
	"context"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/binarygame/microservices/users/internal/usecase"
	"gitlab.com/binarygame/microservices/users/internal/usecase/mocks"
	"gitlab.com/binarygame/microservices/users/pkg/errdefs"
	"gitlab.com/binarygame/microservices/users/pkg/models"
)

var loggerOutput bytes.Buffer

func TestCreate(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(&loggerOutput, nil))

	// Mocks
	userRepo := new(mocks.MockUsersRepository)
	telemetry := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)

	t.Run("success", func(t *testing.T) {
		var emoji, nickname string
		emoji = "👀"
		nickname = "test"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("InsertUser", mock.Anything, mock.Anything).Return(nil).Once()
		telemetry.On("IncrementCounter", mock.Anything, mock.AnythingOfType("string"), mock.AnythingOfType("int")).Return(nil)
		mockSpan.On("End").Return()
		uc := usecase.New(userRepo, logger, telemetry)

		user, err := uc.Create(context.TODO(), nickname, emoji)
		assert.NoError(t, err)
		assert.NotEmpty(t, user)
		assert.Equal(t, nickname, user.Nickname)
		assert.Equal(t, emoji, user.Emoji)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("invalid user data", func(t *testing.T) {
		var emoji, nickname string
		emoji = "emoji"
		nickname = "test"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)

		uc := usecase.New(userRepo, logger, telemetry)
		user, err := uc.Create(context.TODO(), nickname, emoji)

		assert.ErrorIs(t, err, errdefs.ErrInvalidUserData)
		assert.Nil(t, user)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("database failed", func(t *testing.T) {
		var emoji, nickname string
		emoji = "👀"
		nickname = "test"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("InsertUser", mock.Anything, mock.Anything).Return(errdefs.ErrDatabaseFailed).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		user, err := uc.Create(context.TODO(), nickname, emoji)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)
		assert.Nil(t, user)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})
}

func TestEdit(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(&loggerOutput, nil))

	// Mocks
	userRepo := new(mocks.MockUsersRepository)
	telemetry := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)

	validUser, err := models.New("test", "👀")
	assert.NoError(t, err)

	t.Run("success", func(t *testing.T) {
		var emoji, nickname string
		emoji = "👽"
		nickname = "newtestnick"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.Anything).Return(validUser, nil).Once()
		userRepo.On("UpdateUser", mock.Anything, mock.Anything).Return(nil).Once()
		mockSpan.On("End").Return()
		uc := usecase.New(userRepo, logger, telemetry)

		err = uc.Edit(context.TODO(), validUser.ID, nickname, emoji)
		assert.NoError(t, err)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("invalid user data", func(t *testing.T) {
		var emoji, nickname string
		emoji = "emoji"
		nickname = "test"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.Anything).Return(validUser, nil).Twice()
		uc := usecase.New(userRepo, logger, telemetry)

		err = uc.Edit(context.TODO(), validUser.ID, nickname, emoji)
		assert.ErrorIs(t, err, errdefs.ErrInvalidUserData)

		emoji = "👽"
		nickname = "newtestnicknamebiglength"

		err = uc.Edit(context.TODO(), validUser.ID, nickname, emoji)
		assert.ErrorIs(t, err, errdefs.ErrInvalidUserData)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("database failed before update", func(t *testing.T) {
		var emoji, nickname string
		emoji = "👽"
		nickname = "newtestnick"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.Anything).Return(nil, errdefs.ErrDatabaseFailed).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		err := uc.Edit(context.TODO(), validUser.ID, nickname, emoji)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("database failed on update", func(t *testing.T) {
		var emoji, nickname string
		emoji = "👽"
		nickname = "newtestnick"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.Anything).Return(validUser, nil).Once()
		userRepo.On("UpdateUser", mock.Anything, mock.Anything).Return(errdefs.ErrDatabaseFailed).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		err = uc.Edit(context.TODO(), validUser.ID, nickname, emoji)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("user does not exist", func(t *testing.T) {
		var emoji, nickname string
		emoji = "👽"
		nickname = "newtestnick"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.Anything).Return(nil, nil).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		err := uc.Edit(context.TODO(), validUser.ID, nickname, emoji)
		assert.ErrorIs(t, err, errdefs.ErrUserNotFound)

		telemetry.AssertExpectations(t)
		userRepo.AssertExpectations(t)
	})
}

func TestAllowedEmojis(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(&loggerOutput, nil))

	// Mocks
	userRepo := new(mocks.MockUsersRepository)
	telemetry := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)
	telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
	mockSpan.On("End").Return()

	uc := usecase.New(userRepo, logger, telemetry)
	result, err := uc.AllowedEmojis(context.TODO())

	assert.NoError(t, err)
	assert.Equal(t, models.ValidEmojis, result)

	userRepo.AssertExpectations(t)
	telemetry.AssertExpectations(t)
}

func TestGet(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(&loggerOutput, nil))

	// Mocks
	userRepo := new(mocks.MockUsersRepository)
	telemetry := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)

	validUser, err := models.New("test", "👀")
	assert.NoError(t, err)

	t.Run("success", func(t *testing.T) {
		userId := validUser.ID

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.AnythingOfType("string")).Return(validUser, nil).Once()
		mockSpan.On("End").Return()
		uc := usecase.New(userRepo, logger, telemetry)

		user, err := uc.Get(context.TODO(), userId)
		assert.NoError(t, err)
		assert.NotNil(t, user)
		assert.Equal(t, userId, user.ID)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("user does not exist", func(t *testing.T) {
		userId := "1"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.AnythingOfType("string")).Return(nil, errdefs.ErrUserNotFound).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		user, err := uc.Get(context.TODO(), userId)
		assert.ErrorIs(t, err, errdefs.ErrUserNotFound)
		assert.Nil(t, user)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("database failed", func(t *testing.T) {
		userId := "1"

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUserById", mock.Anything, mock.AnythingOfType("string")).Return(nil, errdefs.ErrDatabaseFailed).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		user, err := uc.Get(context.TODO(), userId)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)
		assert.Nil(t, user)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})
}

func TestGetMultiple(t *testing.T) {
	logger := slog.New(slog.NewTextHandler(&loggerOutput, nil))

	// Mocks
	userRepo := new(mocks.MockUsersRepository)
	telemetry := new(mocks.MockTelemetryService)
	mockSpan := new(mocks.MockTelemetrySpan)

	validUser, err := models.New("test", "👀")
	assert.NoError(t, err)

	t.Run("success", func(t *testing.T) {
		userIds := []string{"1", "2"}

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUsersByIds", mock.Anything, mock.Anything).Return([]*models.User{validUser, validUser}, nil).Once()
		mockSpan.On("End").Return()
		uc := usecase.New(userRepo, logger, telemetry)

		users, err := uc.GetMultiple(context.TODO(), userIds)
		assert.NoError(t, err)
		assert.NotNil(t, users)
		assert.Equal(t, len(userIds), len(users))

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})

	t.Run("database failed", func(t *testing.T) {
		userIds := []string{"1", "2"}

		telemetry.On("Record", mock.Anything, mock.AnythingOfType("string"), mock.Anything).Return(mockSpan)
		userRepo.On("GetUsersByIds", mock.Anything, mock.Anything).Return(nil, errdefs.ErrDatabaseFailed).Once()
		uc := usecase.New(userRepo, logger, telemetry)

		users, err := uc.GetMultiple(context.TODO(), userIds)
		assert.ErrorIs(t, err, errdefs.ErrDatabaseFailed)
		assert.Nil(t, users)

		userRepo.AssertExpectations(t)
		telemetry.AssertExpectations(t)
	})
}
